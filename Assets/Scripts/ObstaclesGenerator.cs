﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesGenerator : MonoBehaviour
{
    #region Public Fields
    [Header("Obstacles Generation Properties")]
    public GameObject obstaclePrefab;
    [HideInInspector]
    public float distanceToSpaceBorder;
    #endregion

    #region Private Fields
    Vector3 confindedSpaceCenter;
    float obstacleBound;
    #endregion

    #region Unity CallBacks
    void Start()
    {
        confindedSpaceCenter = GetSpacePosition("Space");
        distanceToSpaceBorder = GetDistanceToSpaceBorder(confindedSpaceCenter
                                                         , "SpaceBorder");
        obstacleBound = GetObstacleBound();
    }

    // Update is called once per frame
    void Update()
    {

    }
    #endregion

    #region Private Methods
    /// <summary>
    /// Gets the Space Object Position (our scenario area)
    /// </summary>
    private Vector3 GetSpacePosition(string spaceObjectTag)
    {
        return GameObject.FindGameObjectWithTag(spaceObjectTag)
                         .transform.position;
    }
    // TODO : Add cases for different shaped space areas rather than cubes only
    /// <summary>
    /// Returns the distance from the space center to any of its borders
    /// Given that the space is a cube
    /// </summary>
    private float GetDistanceToSpaceBorder(Vector3 spacePosition, string spaceBorderTag)
    {
        Vector3 spaceBorder = GameObject.FindGameObjectWithTag(spaceBorderTag).transform.position;
        return Vector3.Distance(spacePosition, spaceBorder);
    }
    /// <summary>
    /// Finds the bound of a single Obstacle, used in circle fitting
    /// </summary>
    private float GetObstacleBound()
    {

        return obstaclePrefab.GetComponent<MeshRenderer>().bounds.size.x;
    }
    /// <summary>
    /// Searchs for any obstacle in the scene by its tag and destroys it
    /// </summary>
    private IEnumerator DestroyObstaclesCoroutine(string obstaclesTag)
    {
        GameObject[] obstacles = GameObject.FindGameObjectsWithTag(obstaclesTag);
        if (obstacles != null)
        {
            for (int i = 0; i < obstacles.Length; i++)
            {
                Destroy(obstacles[i]);
                yield return new WaitForSeconds(0.001f);
            }
        }

    }
    /// <summary>
    /// Generates Obstacles in Circular shapes
    /// </summary>
    /// <param name="maxObsNum"></param>
    /// <param name="distanceToWall"></param>
    /// <returns></returns>
    private IEnumerator GenerateObstaclesCoroutine(int maxObsNum, float obstacleBound, float distanceToSpaceBorder)
    {
        float currentCircleRadius = 0;
        int remainingObstacles = maxObsNum;
        int currentCircleObsCount = 0;
        // Continues generating obstacles while there is remaining ones
        while (remainingObstacles > 0)
        {
            // TODO : Enhance randomness for circle fitting algorithm radius
            #region How many obstacles fit in a circle 
            /* To make sure that between each obstacle's circle 
             * there is space that is more than the obstacle bound
             * to avoid obstacles collision and a random value 
             * to make each circle with a different radius
             */
            currentCircleRadius = currentCircleRadius + obstacleBound + Random.Range(2, 4);
            /* If Radius Value exceeds the space border then
             * stop obstacles generation and exit to avoid obstacles 
             * generation outside the confined space 
             * even if there is obstacles remaining to generate
             * */
            if (currentCircleRadius >= distanceToSpaceBorder - obstacleBound)
                break;
            int howManyObstaclesFit = GetObstaclesCountForACircle(currentCircleRadius, obstacleBound);
            #region Remaining Obstacles Calculation
            /* If the remaining obstacles are larger 
             * that the obstacles for the current circle
            */
            if (remainingObstacles > howManyObstaclesFit)
            {

                remainingObstacles -= howManyObstaclesFit;
                currentCircleObsCount = howManyObstaclesFit;
            }
            else
            {
                currentCircleObsCount = remainingObstacles;
                remainingObstacles = 0;
            }
            #endregion
            #endregion
            CircleFittingAlgorithm(currentCircleRadius, currentCircleObsCount);
            yield return new WaitForSeconds(0.01f);
        }
    }
    // TODO : Add more randomness and control for circle fitting
    /// <summary>
    /// Finds the number of obstacles that fits 
    /// a circle with a given object bounds and radius
    /// </summary>
    private int GetObstaclesCountForACircle(float radius, float obstacleBound)
    {
        float circumference = radius * 2 * Mathf.PI;
        /* We divide the circle circumference 
         * by the object's bound which resembles
         * how many obstacles exactly fit in this circle
         * then we add a random range to leave 
         * spaces between each object in a circle for later path finding
         */
        return (int)(circumference / (obstacleBound + Random.Range(3, 5f)));
    }
    private void CircleFittingAlgorithm(float circleRadius, int obstaclesCount)
    {
        for (int i = 0; i < obstaclesCount; i++)
        {
            var angle = (i * Mathf.PI * 2 / obstaclesCount);
            float sin = Mathf.Sin(angle);
            float cos = Mathf.Cos(angle);
            Vector3 obstaclePosition = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * (circleRadius);
            GameObject newObstacle = Instantiate(obstaclePrefab, obstaclePosition, Quaternion.identity);
        }
    }
    #endregion

    #region Public Methods
    #region Obstacles Related Methods
    public void DestroyAllObstacles(string obstacleTag)
    {
        StartCoroutine(DestroyObstaclesCoroutine(obstacleTag));
    }
    // TODO : Add more obstacles fitting algorithms or enhance instead of circle fitting
    public void GenerateObstacles(int minObstacles, int maxObstacles)
    {
        // Randomize number of obstacles
        int randomObstaclesCount = Random.Range(minObstacles, maxObstacles);
        StartCoroutine(GenerateObstaclesCoroutine(randomObstaclesCount,
                                                  obstacleBound,
                                                  distanceToSpaceBorder));
    }
    #endregion
    #endregion
}
