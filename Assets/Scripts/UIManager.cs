﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{

    // TODO : Save Min & Max Values when changed so that when the player re-opens the game will find his values
    #region Private Fields
    #region UI Elements
    [Header("UI Elements")]
    [SerializeField]
    private Button generateObsBtn;
    [SerializeField]
    private Text minObstaclesValue;
    [SerializeField]
    private Text maxObstaclesValue;
    #endregion
    private SpacePathFinder spacePathFinder;
    private ObstaclesGenerator obstaclesGenerator;
    #endregion

    #region Unity CallBacks
    void Start()
    {
        spacePathFinder = FindObjectOfType<SpacePathFinder>();
        obstaclesGenerator = FindObjectOfType<ObstaclesGenerator>();
    }
    void Update()
    {

    }
    #endregion

    #region Public Methods
    #region Button Callbacks
    //TODO: Need to handle multiple button presses,disable each button till its goal is done then re-enable it 
    public void GenerateObsBtnClk()
    {
        obstaclesGenerator.DestroyAllObstacles("Obstacle");
        obstaclesGenerator.GenerateObstacles(int.Parse(minObstaclesValue.text)
                                       , int.Parse(maxObstaclesValue.text));
    }
    public void FindPathBtnClk()
    {
        spacePathFinder.DestroyPathFindingGameObjs("SpaceShip", "Target", "PathWayLine");
        StartCoroutine(spacePathFinder.DrawPathAndFollowTarget(spacePathFinder.InstantiateSpasceShip
                                                                          (obstaclesGenerator
                                                                          .distanceToSpaceBorder, 0.3f),
                                                               spacePathFinder.InstantiateTarget
                                                                          (obstaclesGenerator
                                                                          .distanceToSpaceBorder, 0.3f)));
    }
    // TODO : Min & Max buttons values to be limited to avoid Invalid Values, e.g lower than zero values
    #region Minimum Obstacles Btns
    public void DecrementMinObsBtnClk()
    {
        minObstaclesValue.text = ((int.Parse(minObstaclesValue.text) - 1)).ToString();
    }
    public void IncrementMinObsBtnClk()
    {
        minObstaclesValue.text = ((int.Parse(minObstaclesValue.text) + 1)).ToString();
    }
    #endregion

    #region Maximum Obstacles Btns
    public void DecrementMaxObsBtnClk()
    {
        maxObstaclesValue.text = ((int.Parse(maxObstaclesValue.text) - 1)).ToString();
    }
    public void IncrementMaxObsBtnClk()
    {
        maxObstaclesValue.text = ((int.Parse(maxObstaclesValue.text) + 1)).ToString();
    }
    #endregion

    #endregion
    #endregion
}
