﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

//TODO: Make unity's navmesh creation dynamic
//TODO: Try Other Open Source path finding algorithms
public class SpacePathFinder : MonoBehaviour
{
    #region Public Fields
    [Header("Path Finding Prefabs")]
    public GameObject spaceShipPrefab;
    public GameObject targetPrefab;
    public GameObject linePrefab;
    #endregion

    #region Unity Callbacks
    void Start()
    {

    }
    void Update()
    {

    }
    #endregion

    #region Public Methods

    #region Path Finding Methods
    /// <summary>
    /// Follows the target & Returns the waypoints between the spaceship & the target 
    /// </summary>
    public IEnumerator DrawPathAndFollowTarget(GameObject spaceShip, GameObject target)
    {
        GameObject lineGameobj = Instantiate(linePrefab);
        // Line Rendering
        LineRenderer line = lineGameobj.GetComponent<LineRenderer>();
        NavMeshPath path = new NavMeshPath();
        // Calculates the path between the spaceship & the target.
        spaceShip.GetComponent<NavMeshAgent>()
                 .CalculatePath(target.transform.position
                                , path);
        // Follows the target

        spaceShip.GetComponent<NavMeshAgent>()
                 .SetDestination(target.transform.position);
        yield return new WaitForEndOfFrame();

        line.positionCount = path.corners.Length;
        line.SetPosition(0, spaceShip.transform.position);
        for (var i = 1; i < path.corners.Length; i++)
        {
            line.SetPosition(i, path.corners[i]);
        }
        Debug.Log(path.corners.Length);
        //return path.corners;
    }
    //TODO: Need to relocate instantiated spaceship when the random position is colliding with an obstacle
    //TODO: Need to make positioning algorithm to ensure that the spaceship and the target are placed in positions where they will find obstacles in the path between them
    /// <summary>
    /// Create the spaceship gameobject with consideration to the confined space we have, our playing area
    /// </summary>
    public GameObject InstantiateSpasceShip(float distanceToSpaceBorder, float SpaceBorderMargin)
    {
        float positioningLimit = distanceToSpaceBorder - SpaceBorderMargin;
        GameObject spaceShip = Instantiate(spaceShipPrefab
                                           , new Vector3(Random.Range(-positioningLimit, positioningLimit)
                                                         , 0, Random.Range(-positioningLimit, positioningLimit))
                                           , Quaternion.identity);
        return spaceShip;
    }
    /// <summary>
    /// 
    /// </summary>
    public void DestroyPathFindingGameObjs(string spaceShipTag, string targetTag, string pathwayLineTag)
    {
        #region Destroy SpaceShip ( Navigation Agent )
        GameObject spaceShip = GameObject.FindGameObjectWithTag(spaceShipTag);
        if (spaceShip != null)
            Destroy(spaceShip);
        #endregion

        #region Destroy Target
        GameObject target = GameObject.FindGameObjectWithTag(targetTag);
        if (target != null)
            Destroy(target);
        #endregion

        #region Destroy WayPoints Line
        GameObject pathwayLine = GameObject.FindGameObjectWithTag(pathwayLineTag);
        if (pathwayLine != null)
            Destroy(pathwayLine);
        #endregion

    }
    #endregion
    /// <summary>
    /// Instantiate target Gameobject with consideration to the confined space
    /// </summary>
    public GameObject InstantiateTarget(float distanceToSpaceBorder, float SpaceBorderMargin)
    {
        float positioningLimit = distanceToSpaceBorder - SpaceBorderMargin;
        GameObject target = Instantiate(targetPrefab
                                           , new Vector3(Random.Range(-positioningLimit, positioningLimit)
                                                         , 0, Random.Range(-positioningLimit, positioningLimit))
                                           , Quaternion.identity);
        return target;
    }
    #endregion
}
